#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <sensor_msgs/PointCloud2.h>
#include <dji_sdk/SendDataToRemoteDevice.h>
ros::ServiceClient ipad;

void cloud_cb(const sensor_msgs::PointCloud2ConstPtr& msg)
{
    uint32_t cnt = msg->width;
    uint32_t rO = 20;
    uint32_t xO =  0;
    uint32_t yO =  4;
    uint32_t zO =  8;
    uint32_t pcnt = 0;
    float psum = 0;
    float pdist = -1;
    if(cnt>0)
    {
       for(int i = 0;i<msg->width;i++)
       {
            uint16_t *ptr1 = (uint16_t*)(&(msg->data[msg->point_step*i + rO]));
            uint16_t ring = ptr1[0];
            if(ring>=7 && ring <= 8) //-1 => +2 deg
            {
                float *ptr2 = (float*)(&(msg->data[msg->point_step*i + xO]));
                float x = ptr2[0];
                float y = ptr2[1];
                float z = ptr2[2];
                float d = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
                if(!std::isnan(d))
                {
                    psum += d;
                    pcnt++;
                }
            }
       }
       if(pcnt>0)
        pdist = psum / ((float)pcnt);
       else
        pdist = -1;
    }
    else
    {
        pdist = -1;
    }
    dji_sdk::SendDataToRemoteDevice srv;
    srv.request.data.assign((uint8_t*)(&pdist), (uint8_t*)(&pdist)+4);
    std::cout << "D:" << pdist << "cnt" << pcnt << std::endl;
    ipad.call(srv);
}

int main (int argc, char ** argv)
{
    ros::init(argc, argv, "lidar_height");
    ros::NodeHandle nh;
    ipad = nh.serviceClient<dji_sdk::SendDataToRemoteDevice>("/dji_sdk/send_data_to_remote_device", true);  
    ros::Subscriber sub = nh.subscribe("/lidar/velodyne_points", 1, cloud_cb);
    
    ros::spin();   
}
