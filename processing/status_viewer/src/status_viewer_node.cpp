#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <vectornav/gps.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <boost/filesystem.hpp>
#include <ecl/threads.hpp>
#include <stdexcept>

using ecl::Mutex;

double lat, lon;
bool record;
cv::Mat img;
unsigned int imageCnt;
double dist;
Mutex imageLock;
Mutex gpsLock;
void imageCallBack(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    imageLock.lock();
    cv_bridge::toCvShare(msg, "bgr8")->image.copyTo(img);
    imageLock.unlock();
    imageCnt++;
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}

void gpsCallBack(const vectornav::gpsConstPtr& msg)
{
//    ROS_INFO("Received GPS position: %f %f", msg->latitude, msg->longitude);
    gpsLock.lock();
    lat = msg->LLA.x;
    lon = msg->LLA.y;
    gpsLock.unlock();
}

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

void recordCallBack(const std_msgs::BoolConstPtr& msg)
{
    record = msg->data;
}

int main(int argc, char **argv)
{
  imageCnt = 0;
  ros::init(argc, argv, "status_viewer");
  ros::NodeHandle nh;
  cv::namedWindow("View", (CV_WINDOW_NORMAL|CV_WINDOW_KEEPRATIO|CV_GUI_EXPANDED));
  cv::setWindowProperty("View", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
  cv::startWindowThread();
  image_transport::ImageTransport it(nh);
  image_transport::Subscriber sub = it.subscribe("/camera/image_raw", 1, imageCallBack);
  ros::Subscriber gpsSub_ = nh.subscribe("/vectornav/gps", 10, &gpsCallBack);
  ros::Subscriber recordSub_ = nh.subscribe("IO3", 10, &recordCallBack);
  while(ros::ok())
  {
    if(img.dims>0)
    {
        cv::Mat scratch;
        imageLock.lock();
        img.copyTo(scratch);
        imageLock.unlock();
        char str[64];
        char rstr[8];
        std::string cmd = "pgrep rosbag -c";
        std::string resp = exec(cmd.c_str());
        if(atoi(resp.c_str())>0) 
          sprintf(rstr,"Record");
        else
          sprintf(rstr,"Pause");
        boost::filesystem::space_info si = boost::filesystem::space("/home/odroid/");
        sprintf(str, "%s %03lluGB", rstr, si.available>>30);
        cv::putText(scratch, str, cv::Point2f(100,200), cv::FONT_HERSHEY_SIMPLEX, 5,  cv::Scalar(0,255,0,255), 24);
        cv::putText(scratch, str, cv::Point2f(100,200), cv::FONT_HERSHEY_SIMPLEX, 5,  cv::Scalar(0,0,255,255), 16);
        gpsLock.lock();
        sprintf(str, "%0.5f %0.5f", lat, lon);
        gpsLock.unlock();
        cv::putText(scratch, str, cv::Point2f(100,img.rows-100), cv::FONT_HERSHEY_SIMPLEX, 5,  cv::Scalar(0,255,0,255), 24);
        cv::putText(scratch, str, cv::Point2f(100,img.rows-100), cv::FONT_HERSHEY_SIMPLEX, 5,  cv::Scalar(0,0,255,255), 16);

        cv::imshow("View", scratch);

    }
    ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.5));
  }
  cv::destroyWindow("View");
}
