#include <ros/ros.h>
#include <rosbag/recorder.h>
#include <ros/callback_queue.h>
#include <dji_sdk/RCChannels.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdexcept>
#include <signal.h>

bool record;
pid_t pid;

void RCCallBack(const dji_sdk::RCChannelsConstPtr& msg)
{
    float gear = msg->gear;
    if(gear==-4545) //Record
    {
	    record = true;
    }
    else
    {
	    record = false;
    }
}

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "online_bag");
  ros::NodeHandle nh;
  ros::Subscriber rcSub_ = nh.subscribe("/dji_sdk/rc_channels", 10, &RCCallBack);
  record = false;
  bool lastRecord = false;
//  options.split = true;
//  options.max_size = 2000;
//  std::string cmd = "rosbag record /vectornav/gps";
  std::string cmd = "rosbag record --split --size=2000 -a -x \"/lidar/velodyne_points|/camera/image\"";
  
  while(ros::ok())
  {
        if(record!=lastRecord)
        {
            if(record)
            {
              pid = fork(); // create child process
              if(pid==0)
              {
                execl("/bin/sh", "sh", "-c", cmd.c_str(), (char *) 0); // run the command
                exit(1);
              }
            }
            else
            {
              char buf[128];
              sprintf(buf, "pgrep -P %d", pid);
              std::string c1 = exec(buf);
              sprintf(buf, "pgrep -P %s", c1.c_str());
              std::string c2 = exec(buf);
              int spid = atoi(c2.c_str());
              kill(spid, SIGINT);
            }
        }
        lastRecord = record;
        ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.5));
  }
  ros::shutdown();
}

